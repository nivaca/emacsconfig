;;; myedit.el -*- lexical-binding: t; -*-

(eval-when-compile (require 'cl-lib)) ;; Require Common Lisp (for "case" etc.)

;; A big contributor to startup times is garbage collection. We up the gc
;; threshold to temporarily prevent it from running, then reset it later by
;; enabling `gcmh-mode'. Not resetting it will cause stuttering/freezes.
(setq gc-cons-threshold most-positive-fixnum)

;; Needs:
;;   git checkout -b native-comp origin/feature/native-comp
;;.  /configure --with-nativecomp
(setq comp-deferred-compilation t)



(defconst IS-MAC     (eq system-type 'darwin))
(defconst IS-LINUX   (eq system-type 'gnu/linux))
(defconst IS-WINDOWS (memq system-type '(cygwin windows-nt ms-dos)))

;; for all computers
(setq user-emacs-directory "~/emacs/")
(setq user-lisp-directory "~/emacs/mylisp/")

(let ((default-directory user-emacs-directory))
  (normal-top-level-add-subdirs-to-load-path)
  )

(setenv "PATH" (concat (getenv "PATH") ":/usr/local/bin"))
(setq exec-path (append exec-path '("/usr/local/bin")))


;;; --------------------------------------------------------
;;; Turn off the annoying crap immediately
(setq backup-inhibited t
      auto-save-default t
      dabbrev-case-distinction nil
      dabbrev-case-fold-search nil
      vc-follow-symlinks t ;; Follow symbolic links
      echo-keystrokes 0.1
      disabled-command-function nil
      global-auto-revert-mode t ;;auto load changed files
      large-file-warning-threshold 536870911)



;;; Emacs core configuration

;; lo', longer logs ahoy
(setq message-log-max 8192)

;; Reduce debug output, well, unless we've asked for it.
(setq debug-on-error nil
      jka-compr-verbose nil)


;; ==================== UTF-8 =======================
;; Contrary to what many Emacs users have in their configs, you really don't
;; need more than this to make UTF-8 the default coding system:
(when (fboundp 'set-charset-priority)
  (set-charset-priority 'unicode))       ; pretty
(prefer-coding-system 'utf-8)            ; pretty
(setq locale-coding-system 'utf-8)       ; please
(setq selection-coding-system 'utf-8)


;; Disable warnings from legacy advice system. They aren't useful, and what can
;; we do about them, besides changing packages upstream?
(setq ad-redefinition-action 'accept)


;; Make apropos omnipotent. It's more useful this way.
(setq apropos-do-all t)


;; A second, case-insensitive pass over `auto-mode-alist' is time wasted, and
;; indicates misconfiguration (or that the user needs to stop relying on case
;; insensitivity).
(setq auto-mode-case-fold nil)


;; Less noise at startup. The dashboard/empty scratch buffer is good enough.
(setq inhibit-startup-message t
      inhibit-startup-echo-area-message user-login-name
      inhibit-default-init t
      ;; Avoid pulling in many packages by starting the scratch buffer in
      ;; `fundamental-mode', rather than, say, `org-mode' or `text-mode'.
      initial-major-mode 'fundamental-mode
      initial-scratch-message nil)


;; Get rid of "For information about GNU Emacs..." message at startup, unless
;; we're in a daemon session, where it'll say "Starting Emacs daemon." instead,
;; which isn't so bad.
(unless (daemonp)
  (advice-add #'display-startup-echo-area-message :override #'ignore))


;; Emacs "updates" its ui more often than it needs to, so we slow it down
;; slightly from 0.5s:
(setq idle-update-delay 1.0)


;; Emacs is essentially one huge security vulnerability, what with all the
;; dependencies it pulls in from all corners of the globe. Let's try to be at
;; least a little more discerning.
(setq gnutls-verify-error (not (getenv "INSECURE"))
      gnutls-algorithm-priority
      (when (boundp 'libgnutls-version)
        (concat "SECURE128:+SECURE192:-VERS-ALL"
                (if (and (not IS-WINDOWS)
                         (not (version< emacs-version "26.3"))
                         (>= libgnutls-version 30605))
                    ":+VERS-TLS1.3")
                ":+VERS-TLS1.2"))
      ;; `gnutls-min-prime-bits' is set based on recommendations from
      ;; https://www.keylength.com/en/4/
      gnutls-min-prime-bits 3072
      tls-checktrust gnutls-verify-error
      ;; Emacs is built with `gnutls' by default, so `tls-program' would not be
      ;; used in that case. Otherwise, people have reasons to not go with
      ;; `gnutls', we use `openssl' instead. For more details, see
      ;; https://redd.it/8sykl1
      tls-program '("openssl s_client -connect %h:%p -CAfile %t -nbio -no_ssl3 -no_tls1 -no_tls1_1 -ign_eof"
                    "gnutls-cli -p %p --dh-bits=3072 --ocsp --x509cafile=%t \
--strict-tofu --priority='SECURE192:+SECURE128:-VERS-ALL:+VERS-TLS1.2:+VERS-TLS1.3' %h"
                    ;; compatibility fallbacks
                    "gnutls-cli -p %p %h"))



;;
;;; Minibuffer

;; Allow for minibuffer-ception. Sometimes we need another minibuffer command
;; _while_ we're in the minibuffer.
(setq enable-recursive-minibuffers t)

;; Show current key-sequence in minibuffer, like vim does. Any feedback after
;; typing is better UX than no feedback at all.
(setq echo-keystrokes 0.02)

;; Expand the minibuffer to fit multi-line text displayed in the echo-area. This
;; doesn't look too great with direnv, however...
(setq resize-mini-windows 'grow-only
      ;; But don't let the minibuffer grow beyond this size
      max-mini-window-height 0.15)

;; Disable help mouse-overs for mode-line segments (i.e. :help-echo text).
;; They're generally unhelpful and only add confusing visual clutter.
(setq mode-line-default-help-echo nil
      show-help-function nil)

;; Typing yes/no is obnoxious when y/n will do
(fset #'yes-or-no-p #'y-or-n-p)

;; Try really hard to keep the cursor from getting stuck in the read-only prompt
;; portion of the minibuffer.
(setq minibuffer-prompt-properties '(read-only t intangible t cursor-intangible t face minibuffer-prompt))
(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

;; Don't display messages in the minibuffer when using the minibuffer
(defmacro nv-silence-motion-key (command key)
  (let ((key-command (intern (format "doom/silent-%s" command))))
    `(progn
       (defun ,key-command ()
         (interactive)
         (ignore-errors (call-interactively ',command)))
       (define-key minibuffer-local-map (kbd ,key) #',key-command))))
(nv-silence-motion-key backward-delete-char "<backspace>")
(nv-silence-motion-key delete-char "<delete>")

(setq message-log-max t)

;; Share clipboard with system
(setq select-enable-clipboard t)


;; Remove command line options that aren't relevant to our current OS; that
;; means less to process at startup.
(unless IS-MAC   (setq command-line-ns-option-alist nil))
(unless IS-LINUX (setq command-line-x-option-alist nil))



;;
;;; Optimizations

;; Disable bidirectional text rendering for a modest performance boost. I've set
;; this to `nil' in the past, but the `bidi-display-reordering's docs say that
;; is an undefined state and suggest this to be just as good:
(setq-default bidi-display-reordering 'left-to-right
              bidi-paragraph-direction 'left-to-right)


;; Reduce rendering/line scan work for Emacs by not rendering cursors or regions
;; in non-focused windows.
(setq-default cursor-in-non-selected-windows nil)
(setq highlight-nonselected-windows nil)


;; More performant rapid scrolling over unfontified regions. May cause brief
;; spells of inaccurate syntax highlighting right after scrolling, which should
;; quickly self-correct.
(setq fast-but-imprecise-scrolling t)


;; Resizing the Emacs frame can be a terribly expensive part of changing the
;; font. By inhibiting this, we halve startup times, particularly when we use
;; fonts that are larger than the system default (which would resize the frame).
(setq frame-inhibit-implied-resize t)


;; Don't ping things that look like domain names.
(setq ffap-machine-p-known 'reject)


;; Font compacting can be terribly expensive, especially for rendering icon
;; fonts on Windows. Whether it has a notable affect on Linux and Mac hasn't
;; been determined, but we inhibit it there anyway.
(setq inhibit-compacting-font-caches t)


;; Remove command line options that aren't relevant to our current OS; means
;; slightly less to process at startup.
(unless IS-MAC   (setq command-line-ns-option-alist nil))
(unless IS-LINUX (setq command-line-x-option-alist nil))


;; Delete files to trash on macOS, as an extra layer of precaution against
;; accidentally deleting wanted files.
(setq delete-by-moving-to-trash IS-MAC)


;; HACK `tty-run-terminal-initialization' is *tremendously* slow for some
;;      reason. Disabling it completely could have many side-effects, so we
;;      defer it until later, at which time it (somehow) runs very quickly.
(unless (daemonp)
  (advice-add #'tty-run-terminal-initialization :override #'ignore)
  (add-hook 'window-setup-hook
            (defun doom-init-tty-h ()
              (advice-remove #'tty-run-terminal-initialization #'ignore)
              (tty-run-terminal-initialization (selected-frame) nil t))))



;; ;; ==================== UTF-8 =======================
;; (setenv "LC_CTYPE" "UTF-8")
;; (setenv "LC_ALL" "en_US.UTF-8")
;; (setenv "LANG" "en_US.UTF-8")
;; (setq locale-coding-system 'utf-8)
;; (set-terminal-coding-system 'utf-8)
;; (set-keyboard-coding-system 'utf-8)
;; (set-selection-coding-system 'utf-8)
;; (prefer-coding-system 'utf-8)
;; (setq buffer-file-coding-system 'utf-8)
;; ;; From Emacs wiki
;; (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))


;; Save whatever’s in the current (system) clipboard before
;; replacing it with the Emacs’ text.
;; https://github.com/dakrone/eos/blob/master/eos.org
(setq save-interprogram-paste-before-kill t)

;; Save clipboard contents into kill-ring before replacing them
(setq save-interprogram-paste-before-kill t)



;; -----------------------------------------------------------------
;; Garbage collection
(defvar better-gc-cons-threshold 67108864 ; 64mb
  "The default value to use for `gc-cons-threshold'.
If you experience freezing, decrease this. If you experience stuttering, increase this.")

(add-hook 'emacs-startup-hook
          (lambda ()
            (if (boundp 'after-focus-change-function)
                (add-function :after after-focus-change-function
                              (lambda ()
                                (unless (frame-focus-state)
                                  (garbage-collect))))
              (add-hook 'after-focus-change-function 'garbage-collect))

            (defun gc-minibuffer-setup-hook ()
              (setq gc-cons-threshold (* better-gc-cons-threshold 2)))

            (defun gc-minibuffer-exit-hook ()
              (garbage-collect)
              (setq gc-cons-threshold better-gc-cons-threshold))

            (add-hook 'minibuffer-setup-hook #'gc-minibuffer-setup-hook)
            (add-hook 'minibuffer-exit-hook #'gc-minibuffer-exit-hook)))

;; -----------------------------------------------------------------

;; =============== Package Management ===============
(load-file (expand-file-name "mypackages.el" user-lisp-directory))


;; --------------------------------------------------
;; Modes and mode groupings
(defmacro hook-into-modes (func modes)
  "Add hook `FUNC' to multiple `MODES'."
  `(dolist (mode-hook, modes)
     (add-hook mode-hook, func)))


;; start with fundamental mode
(setq initial-major-mode 'fundamental-mode)



;; ================= My editor settings ===================
(use-package myedit
  :load-path user-lisp-directory
  )


;; =================== desktop etc. ====================
(use-package mydesktop
  :load-path user-lisp-directory
  :config
  (recentf-cleanup)
  )


;; =============== Flyspell ==================
(use-package myspell
  :load-path user-lisp-directory
  )


;; =============== Flycheck ==================
(use-package flycheck
  :defer y
  :diminish flycheck-mode
  :config
  (setq flycheck-global-modes nil)
  ;; (add-hook 'latex-mode-hook 'flycheck-mode)
  ;;(global-flycheck-mode)
  )


;; ================= AUCTEX =====================
(use-package myauctex
  :load-path user-lisp-directory
  )



;; =================  Parentheses ================
(use-package myparent
  :load-path user-lisp-directory
  )


;; ;; ===================== ansi-term =======================
;; (use-package vterm
;;   :custom
;;   (vterm-shell "fish")
;;   )

;; ================ magit ===============
(use-package mymagit
  :load-path user-lisp-directory
  )


;; =================== ORG-mode ====================
(use-package myorg
  :load-path user-lisp-directory
  )

;; ===================== ediff ==========================
(use-package ediff
  :config (setq ediff-split-window-function 'split-window-horizontally))


;; ================= company ==================
(use-package mycompany
  :if window-system
  :load-path user-lisp-directory
  )


;; ================= dired ==================
(use-package dired
  :defer t
  :bind ([S-f8] . dired)
  :init
  (add-hook 'dired-mode-hook #'all-the-icons-dired-mode)
  )

(use-package dired-narrow
  :defer t
  )

(use-package dired-subtree
  :after dired
  :config
  (bind-key "<tab>" #'dired-subtree-toggle dired-mode-map)
  (bind-key "<backtab>" #'dired-subtree-cycle dired-mode-map)
  )

;; no extra buffers when entering different dirs
(use-package dired-single
  :config
  (add-hook 'dired-mode-hook
            (lambda ()
              (define-key dired-mode-map (kbd "RET") 'dired-single-buffer)
              (define-key dired-mode-map (kbd "<mouse-1>") 'dired-single-buffer-mouse)
              (define-key dired-mode-map (kbd "^")
                (lambda ()
                  (interactive)
                  (dired-single-buffer "..")))))
  )



;; =================== lsp ===================
;; (use-package lsp-mode
;;   :defer t
;;   )

;; (use-package lsp-ivy
;;   :after lsp-mode
;;   )


;; =================== nxml ===================
(use-package myxml
  ;; :disabled
  :load-path user-lisp-directory
  )


;; ================= markdown ===================
(use-package mymarkdown
  :disabled
  :load-path user-lisp-directory
  )


;; ================= treemacs ===================
(use-package mytreemacs
  :load-path user-lisp-directory
  )


;; ===== Garbage Collector Magic Hack ====
(use-package gcmh
  :diminish
  :custom
  ;; Adopt a sneaky garbage collection strategy of waiting until idle time to
  ;; collect; staving off the collector while the user is working.
  (gcmh-idle-delay 5)
  (gcmh-high-cons-threshold (* 16 1024 1024)) ; 16mb
  (gcmh-verbose nil)
  :init
  (gcmh-mode 1))


;; ================= My ivy-counsel ===================
(use-package myivy
  :load-path user-lisp-directory
  )


;; ================== ctrlf isearch ===================
;; (use-package ctrlf
;;   :init
;;   (ctrlf-mode +1)
;;   )


;; ================= My lsp ===================
(use-package mylsp
  :disabled
  :load-path user-lisp-directory
  )

;; ================= Try ===================
(use-package try
  )


;; ================= Scratch ===================
(use-package scratch
  :config
  (defun nv-scratch-buffer-setup ()
    "Add contents to `scratch' buffer and name it accordingly."
    (let* ((mode (format "%s" major-mode))
           (string (concat "Scratch buffer for: " mode "\n\n")))
      (when scratch-buffer
        (save-excursion
          (insert string)
          (goto-char (point-min))
          (comment-region (point-at-bol) (point-at-eol)))
        (forward-line 2))
      (rename-buffer (concat "*Scratch for " mode "*") t)))
  :hook (scratch-create-buffer-hook . prot/scratch-buffer-setup)
  )


;; ================= My functions ===================
(use-package myfunctions
  :load-path user-lisp-directory
  )

;; ================= My aliases ===================
(use-package myaliases
  :load-path user-lisp-directory
  )


;; ============= My display configuration ==========
(use-package mydisplay
  :load-path user-lisp-directory
  )



;; ================= KEY remap ===============
(use-package mykeys
  :load-path user-lisp-directory
  )

;; ================= server ==================
(use-package server
  :config
  (unless (server-running-p)
    (server-start)))





;; -----------------------------------------
;; needed for ivy
(setq ivy-initial-inputs-alist ())



;; =========================================
;; Startup time

;; (defun nv-display-startup-time ()
;;   (message "Emacs loaded in %s with %d garbage collections."
;;            (format "%.2f seconds"
;;                    (float-time
;;                     (time-subtract after-init-time before-init-time)))
;;            gcs-done))

;; (add-hook 'emacs-startup-hook #'nv-display-startup-time)

(provide 'myinit)
;;; myinit.el ends here
