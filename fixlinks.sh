#! /usr/bin/env bash

if [ "$(uname)" == "Darwin" ]; then
  ln -sf /Users/nicolasvaughan/Dropbox/dotfiles/emacs /Users/nicolasvaughan/
  ln -sf /Users/nicolasvaughan/Dropbox/dotfiles/emacs/myinit.el ~/.emacs
  ln -sf /Users/nicolasvaughan/Dropbox/dotfiles/mac/.emacs.d/ ~/
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
  ln -sf ~/Dropbox/dotfiles/emacs ~/emacs
  ln -sf ~/Dropbox/dotfiles/emacs/myinit.el ~/.emacs
  ln -sf ~/Dropbox/dotfiles/.emacs.d/ ~/.emacs.d
fi
