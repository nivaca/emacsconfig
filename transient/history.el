((magit-commit nil
               ("--all"))
 (magit-diff
  ("--no-ext-diff" "--stat"))
 (magit-merge nil)
 (magit-push nil)
 (magit-rebase nil))
