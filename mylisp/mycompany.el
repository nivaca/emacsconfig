;;; mylisp/mycompany.el -*- lexical-binding: t; -*-

(use-package company
  :ensure
  :defer t
  :init
  (global-company-mode)
  :custom
  (company-idle-delay 0.3)
  (company-tooltip-limit 0)
  (company-begin-commands '(self-insert-command))
  (company-transformers '(company-sort-by-occurrence))
  (company-selection-wrap-around t)
  (company-minimum-prefix-length 3)
  (company-selection-wrap-around t)
  ;; (company-dabbrev-downcase nil)
  )

;; (add-hook 'after-init-hook 'global-company-mode)

(use-package company-auctex
  :defer t
  :config
  (company-auctex-init)
  (add-to-list 'company-backends '(company-auctex))
)

(with-eval-after-load 'company
  (add-hook 'latex-mode-hook 'company-mode)
  (add-hook 'lisp-mode-hook 'company-mode)
  )

;; (use-package company-org-roam
;;;; :defer t
;; :config
;; (push 'company-org-roam company-backends))


(provide 'mycompany)
;;; mycompany.el ends here
