;;; mylisp/myprojectile.el -*- lexical-binding: t; -*-

;; ============== projectile ==============
(use-package projectile
  :defer t
  )

(use-package counsel-projectile
  :defer t
  )
(provide 'myprojectile)
