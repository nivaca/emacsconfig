;;; mylisp/mydisplay.el -*- lexical-binding: t; -*-

;; nivaca-pc: desktop casa
;; nivaca-tp: portátil thinkpad x220
;; nivaca-dell: portátil dell

(tool-bar-mode -1)

(use-package menu-bar
  :bind
  ([S-f10] . menu-bar-mode))


(use-package tooltip
  :defer t
  :custom
  (tooltip-mode -1))


(use-package time
  :defer t
  :custom
  (display-time-default-load-average nil)
  (display-time-24hr-format t)
  :config
  (display-time-mode t)
  )


(setq visible-bell nil
      ring-bell-function 'ignore
      font-lock-maximum-decoration t
      truncate-partial-width-windows nil
      minibuffer-message-timeout 10
      column-number-mode t
      ;; pop-up-frames nil
      )

;; Don't resize emacs in steps, it looks weird.
(setq window-resize-pixelwise t
      frame-resize-pixelwise t)

;; Reduce the clutter in the fringes
;; (setq indicate-buffer-boundaries nil
;;       indicate-empty-lines nil)

;; show trailing spaces
(setq show-trailing-whitespace nil)

;; highlight current line
(setq global-hl-line-mode -1)

;; Show me empty lines after buffer end
(set-default 'indicate-empty-lines t)

(global-display-line-numbers-mode t)

;; increase line space for better readability
(setq-default line-spacing 3)

;; 1/4 of the total height
;; (setq max-mini-window-height 0.25)

(when window-system
  (setq frame-title-format '(buffer-file-name "%f" ("%b")))
  (tooltip-mode -1)
  (blink-cursor-mode -1))


;; ++++++++++++++++++++++++++++++++++++++++
;;                  Fonts
;; ++++++++++++++++++++++++++++++++++++++++
(when window-system
  (pcase (system-name)
    ;; PC escritorio casa
    ("nivaca-pc" (set-frame-font "Fantasque Sans Mono 15" nil t))
    ;; Thinkpad
    ("nivaca-tp" (set-frame-font "Fantasque Sans Mono 13"))
    ;; Dell
    ("nivaca-dell" (set-frame-font "Fantasque Sans Mono 14"))
    ;; Mac
    ("nivaca-mac" (set-frame-font "Fantasque Sans Mono 20"))
    )

  ;; ;; Set the fixed pitch face
  ;; (set-face-attribute 'fixed-pitch nil :font "Fantasque Sans Mono" :height 130)

  ;; ;; Set the variable pitch face
  ;; (set-face-attribute 'variable-pitch nil :font "Fantasque Sans Mono" :height 140 :weight 'regular)

  )


;; ++++++++++++++++++++++++++++++++++++++++

;; (setq custom-theme-directory (concat user-emacs-directory "otherlisp/themes"))
;; (load-theme 'gnome2 t)


(use-package base16-theme
  :disabled
  :config
  (load-theme 'base16-flat t))

(use-package doom-themes
  ;; :disabled
  :defer t
  :config
  (setq doom-nord-brighter-comments t)
  (setq doom-one-brighter-comments t)
  (setq doom-one-brighter-modeline t)
  :init
  (load-theme 'doom-one t)
  )

(use-package color-theme-sanityinc-tomorrow
  :disabled
  :defer t
  :init
  (load-theme 'sanityinc-tomorrow-eighties t)
  )

;; pretty-mode
(use-package pretty-mode
  :commands pretty-mode
  :config
  (hook-into-modes #'pretty-mode
                   '(emacs-lisp-mode-hook
                     coffee-mode-hook
                     python-mode-hook
                     ruby-mode-hook
                     haskell-mode-hook
                     latex-mode-hook
                     ))
  )



;; Highlight brackets according to their depth
(use-package rainbow-delimiters
  :ensure
  :config
  (rainbow-delimiters-mode 1)
  )



;; ----------------------------------------------------
;; valid values are t, nil, box, hollow, bar, (bar . WIDTH), hbar,
;; (hbar. HEIGHT); see the docs for set-cursor-type
(setq nv-read-only-color   "gray")
(setq nv-read-only-cursor-type 'hbar)
(setq nv-overwrite-color   "red")
(setq nv-overwrite-cursor-type 'hollow)
(setq nv-normal-color  "yellow")
;; (setq nv-normal-color  "black")
(setq nv-normal-cursor-type '(bar . 2))

(defun nv-set-cursor-according-to-mode ()
  "change cursor color and type according to some minor modes."
  (cond
   (buffer-read-only
    (set-cursor-color nv-read-only-color)
    (setq cursor-type nv-read-only-cursor-type))
   (overwrite-mode
    (set-cursor-color nv-overwrite-color)
    (setq cursor-type nv-overwrite-cursor-type))
   (t
    (set-cursor-color nv-normal-color)
    (setq cursor-type nv-normal-cursor-type))))

(add-hook 'post-command-hook 'nv-set-cursor-according-to-mode)


;; --------------------------------------------
;; This is an optical wrap at the right margin
(global-visual-line-mode t)
(setq visual-line-fringe-indicators
      '(left-curly-arrow right-curly-arrow))

;; --------------------------------------------
(setq global-font-lock-mode 1) ; everything should use fonts
(setq font-lock-maximum-decoration t)

;; Set frame title to file name
(setq frame-title-format
      '((:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name))
                 "%b"))))

;; don't let the cursor go into minibuffer prompt
(setq minibuffer-prompt-properties (quote (read-only t point-entered minibuffer-avoid-prompt face minibuffer-prompt)))



;; =================== diminish ======================
(use-package diminish
  :config
  (eval-after-load "undo-tree" '(diminish 'undo-tree-mode))
  (eval-after-load "guide-key" '(diminish 'guide-key-mode))
  (eval-after-load "smartparens" '(diminish 'smartparens-mode))
  (eval-after-load "guide-key" '(diminish 'guide-key-mode))
  (eval-after-load "eldoc" '(diminish 'eldoc-mode))
  (eval-after-load "company" '(diminish 'company-mode))
  (eval-after-load "flycheck-mode" '(diminish 'flycheck-mode))
  (eval-after-load "ivy" '(diminish 'ivy-mode))
  (eval-after-load "hungry-delete" '(diminish 'hungry-delete-mode))
  (diminish 'python-mode)
  (diminish 'flyspell-mode)
  (diminish 'visual-line-mode)
  )


;; ========== distinguish dashes ============
;; http://emacs.stackexchange.com/questions/9623/tell-a-dash-an-en-dash-and-an-emdash-apart
;;
(let* (
       (glyph-en-dash (make-glyph-code ?\u002D 'font-lock-keyword-face))
       (glyph-em-dash (make-glyph-code ?\u002D 'font-lock-function-name-face)) )
  (when (not buffer-display-table)
    (setq buffer-display-table (make-display-table)))
  (aset buffer-display-table 8211 `[,glyph-en-dash ,glyph-en-dash])
  (aset buffer-display-table 8212 `[,glyph-em-dash ,glyph-em-dash ,glyph-em-dash]))



;; ============= whitespace =============
(use-package whitespace
  :disabled
  :defer t
  :init (setq highlight-indent-guides-method 'character)
  :diminish whitespace-mode
  )


;; ================= beacon ====================
(use-package beacon
  ;; Highlight cursor position in buffer
  :custom
  (beacon-push-mark 10)
  (beacon-color "#cc342b")
  (beacon-blink-delay 0.3)
  (beacon-blink-duration 0.3)
  :init (beacon-mode 1)
  :diminish beacon-mode
  )


;; =============== whitespace mode ======================
(setq whitespace-style (quote (spaces tabs newline space-mark tab-mark newline-mark)))
(setq whitespace-display-mappings
      ;; all numbers are Unicode codepoint in decimal. e.g. (insert-char 182 1)
      '(
        (space-mark 32 [183] [46]) ; 32 SPACE 「 」, 183 MIDDLE DOT 「·」, 46 FULL STOP 「.」
        (newline-mark 10 [182 10]) ; 10 LINE FEED
        (tab-mark 9 [9655 9] [92 9]) ; 9 TAB, 9655 WHITE RIGHT-POINTING TRIANGLE 「▷」
        ))

;; ====================================================
(use-package "window"
  :config
  (defun nv-split-and-follow-horizontally ()
    "Split window below."
    (interactive)
    (split-window-below)
    (other-window 1))
  (defun nv-split-and-follow-vertically ()
    "Split window right."
    (interactive)
    (split-window-right)
    (other-window 1))
  (global-set-key (kbd "C-x 2") 'nv-split-and-follow-horizontally)
  (global-set-key (kbd "C-x 3") 'nv-split-and-follow-vertically))



(use-package awesome-tab
  :load-path "~/emacs/otherlisp/awesome-tab"
  :config
  (defun awesome-tab-hide-tab (x)
    (let ((name (format "%s" x)))
      (or
       (string-prefix-p "*epc" name)
       (string-prefix-p "*helm" name)
       (string-prefix-p "*Compile-Log*" name)
       (string-prefix-p "*lsp" name)
       (and (string-prefix-p "magit" name)
            (not (file-name-extension name)))
       )))
  (setq awesome-tab-style 'bar
	awesome-tab-cycle-scope 'tabs)
  (awesome-tab-mode t)
  )


;; ----------------------------------------------
;;  All the icons
(use-package all-the-icons
  :defer)

(use-package all-the-icons-ivy
  :config
  (all-the-icons-ivy-setup)
  )

(use-package all-the-icons-dired
  :config
  )


;; Show color codes
;;-----------------------------------------------
(use-package rainbow-mode
  )




;; -----------------------------------------------

(defun nv-select-buffer-in-side-window (buffer alist)
  "Display buffer in a side window and select it"
  (let ((window (display-buffer-in-side-window buffer alist)))
    (select-window window)))

(add-to-list 'display-buffer-alist '("\\*\\(?:Warnings\\|Compile-Log\\|Messages\\)\\*"
                                     (nv-select-buffer-in-side-window)
                                     (window-height . 0.20)
                                     (side . bottom)
                                     (slot . -5)
                                     (preserve-size . (nil . t))))

(provide 'mydisplay)
