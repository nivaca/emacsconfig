;;; mylisp/myauctex.el -*- lexical-binding: t; -*-

(use-package auctex
  :defer t
  :mode
  ("\\.tex\\'" . latex-mode)
  ("\\.ltx\\'" . latex-mode)
  :commands
  (latex-mode
   LaTeX-mode
   TeX-mode)
  :hook
  ((LaTeX-mode . LaTeX-preview-setup)
   (LaTeX-mode . flyspell-mode)
   (LaTeX-mode . turn-on-reftex))
  :init
  (setq-default TeX-master nil)
  ;; :config
  ;; (sp-local-pair 'latex-mode "\\begin" "\\end")
  :custom
  (TeX-auto-save t)
  (TeX-parse-self t)
  (TeX-save-query nil)
  (TeX-PDF-mode t)
  (LaTeX-beamer-item-overlay-flag nil)
  (TeX-PDF-mode t)
  (TeX-quote-after-quote nil)
  (TeX-open-quote "\"")
  (TeX-close-quote "\"")
  )


(use-package reftex
  :after auctex
  :diminish reftex-mode)


(add-hook 'LaTeX-mode-hook
          (lambda ()
            (add-to-list 'TeX-command-list
                         '("latexmk" "latexmk -pdf %s" TeX-run-TeX nil t
                           :help "Run latexmk on file"))))


(with-eval-after-load "tex"
  (add-to-list 'TeX-view-program-list '("okular" "/usr/bin/okular %o"))
  (setcdr (assq 'output-pdf TeX-view-program-selection) '("okular")))


(defun TeX-insert-quote ()
  " "
  )



(provide 'myauctex)
