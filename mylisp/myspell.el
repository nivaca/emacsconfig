;;; mylisp/mydisplay.el -*- lexical-binding: t; -*-

;; nivaca-pc: desktop casa
;; nivaca-tp: portátil thinkpad x220
;; nivaca-dell: portátil dell

;; (when (eq system-type 'gnu/linux)
;;   (setq ispell-program-name "/usr/bin/aspell")
;;   )
;; ;; Mac settings
;; (when (eq system-type 'darwin)
;;   ;; > brew install aspell
;;   (setq ispell-program-name "/usr/local/bin/aspell")
;; )


(use-package flyspell
  :defer t
  :config
  (define-key flyspell-mode-map [down-mouse-3] 'flyspell-correct-word)

  (setq ispell-program-name "aspell"
        aspell-dictionary "en_GB-ise-wo_accents"
        aspell-program-name "/usr/bin/aspell"
        ispell-dictionary "en_GB-ise-wo_accents"
        ispell-program-name "/usr/bin/aspell")

  (when IS-MAC
    (progn
      (setq ispell-program-name "aspell"
            aspell-dictionary "en_GB-ise-wo_accents"
            aspell-program-name "/usr/local/bin/aspell"
            ispell-dictionary "en_GB-ise-wo_accents"
            ispell-program-name "/usr/local/bin/aspell")
      (unbind-key "C-." flyspell-mode-map)
      (unbind-key "C-," flyspell-mode-map)
      (bind-key "C-." 'comment-or-uncomment-line-or-region)
      (bind-key "C-," 'comment-or-uncomment-line-or-region)
      )
    )

  (defun make-flyspell-overlay-return-mouse-stuff (overlay)
    (overlay-put overlay 'help-echo nil)
    (overlay-put overlay 'keymap nil)
    (overlay-put overlay 'mouse-face nil))

  (advice-add 'make-flyspell-overlay :filter-return #'make-flyspell-overlay-return-mouse-stuff)

  (add-hook 'LaTeX-mode-hook #'flyspell-mode)
  )



(use-package flyspell-correct-ivy
  :after flyspell
  :bind (:map flyspell-mode-map
              ("C-c $" . flyspell-correct-word-generic)))



(provide 'myspell)
