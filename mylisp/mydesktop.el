;;; mylisp/mydesktop.el -*- lexical-binding: t; -*-

;; Desktop, savaplace, recentf

;; nivaca-pc: desktop casa
;; nivaca-tp: portátil thinkpad x220
;; nivaca-dell: desktop oficina


;; ================ desktop save ===============
(require 'desktop)


(pcase (system-name)
  ;; PC escritorio casa
  ("nivaca-pc" (setq desktop-dirname (concat user-emacs-directory "tmp/pc/")))
  ;; Thinkpad
  ("nivaca-tp" (setq desktop-dirname (concat user-emacs-directory "tmp/tp/")))
  ;; Dell
  ("nivaca-dell" (setq desktop-dirname (concat user-emacs-directory "tmp/dell/")))
  )
;; Mac oficina
(when IS-MAC
  (setq desktop-dirname (concat user-emacs-directory "tmp/mac/")))

(setq desktop-base-file-name      "desktop"
      desktop-path                (list desktop-dirname)
      desktop-save                t
      desktop-files-not-to-save   "^$" ;reload tramp paths
      desktop-load-locked-desktop nil
      desktop-auto-save-timeout   30)

(desktop-save-mode t)

(setq my-lock-file (concat desktop-dirname desktop-base-lock-name))

;; check if lock-file for desktop file exists
;; and accordingly deletes it
(if (file-exists-p my-lock-file)
    (progn
      (message "Deleting destop lock file...")
      (delete-file my-lock-file)))


;; (bind-key* "<C-kp-add>" 'desktop-save-mode nil)


(defun nv-desktop-save ()
  (interactive)
  ;; Don't call desktop-save-in-desktop-dir, as it prints a message.
  (if (eq (desktop-owner) (emacs-pid))
      ;;(message "Saving desktop file...")
      (setq temp nil)
    ;; (desktop-save desktop-path)))
    (desktop-save desktop-dirname)))


(add-hook 'auto-save-hook 'nv-desktop-save)


;; ============== Custom edit file ================
(use-package cus-edit
  :init
  (pcase (system-name)
    ;; PC escritorio casa
    ("nivaca-pc" (setq custom-file (concat user-emacs-directory "tmp/pc/custom.el")))
    ;; Thinkpad
    ("nivaca-tp" (setq custom-file (concat user-emacs-directory "tmp/tp/custom.el")))
    ;; Dell
    ("nivaca-dell" (setq custom-file (concat user-emacs-directory "tmp/dell/custom.el")))
    )
  ;; Mac oficina
  (when IS-MAC
    (setq custom-file (concat user-emacs-directory "tmp/mac/custom.el")))
  :hook
  (after-init . (lambda ()
                  (unless (file-exists-p custom-file)
                    (write-region "" nil custom-file))
                  (load custom-file))))




;; Remember mini-buffer history --------------------------
(use-package savehist
  :init
  (pcase (system-name)
    ;; PC escritorio casa
    ("nivaca-pc" (setq savehist-file (concat user-emacs-directory "tmp/pc/savehist")))
    ;; Thinkpad
    ("nivaca-tp" (setq savehist-file (concat user-emacs-directory "tmp/tp/savehist")))
    ;; Dell
    ("nivaca-dell" (setq savehist-file (concat user-emacs-directory "tmp/dell/savehist")))
    )
  ;; Mac oficina
  (when IS-MAC
    (setq savehist-file (concat user-emacs-directory "tmp/mac/savehist")))
  :custom
  (history-length 1000)
  (savehist-save-minibuffer-history t)
  (savehist-additional-variables '(kill-ring search-ring regexp-search-ring))
  :config
  (savehist-mode 1)
  )





;; =================== saveplace ===================
(use-package saveplace
  :config
  (save-place-mode)
  (pcase (system-name)
    ;; PC escritorio casa
    ("nivaca-pc" (setq save-place-file (concat user-emacs-directory "tmp/pc/saved-places")))
    ;; Thinkpad
    ("nivaca-tp" (setq save-place-file (concat user-emacs-directory "tmp/tp/saved-places")))
    ;; Dell
    ("nivaca-dell" (setq save-place-file (concat user-emacs-directory "tmp/dell/saved-places")))
    )
  ;; Mac oficina
  (when IS-MAC
    (setq save-place-file (concat user-emacs-directory "tmp/mac/saved-places"))
    )
  )



;; ============= recentf stuff ================
(use-package recentf
  :config
  (pcase (system-name)
    ;; PC escritorio casa
    ("nivaca-pc" (setq recentf-save-file (concat user-emacs-directory "tmp/pc/recentf")))
    ;; Thinkpad
    ("nivaca-tp" (setq recentf-save-file (concat user-emacs-directory "tmp/tp/recentf")))
    ;; Dell
    ("nivaca-dell" (setq recentf-save-file (concat user-emacs-directory "tmp/dell/recentf")))
    )
  ;; Mac oficina
  (when IS-MAC
    (setq recentf-save-file (concat user-emacs-directory "tmp/mac/recentf")))
  ;;
  (setq recentf-max-saved-items 300
        recentf-exclude '("/auto-install/" ".recentf" "/repos/" "/elpa/"
                          "\\.mime-example" "\\.ido.last" "COMMIT_EDITMSG"
                          ".gz"
                          "~$" "/tmp/pc/" "/tmp/tp/" "/tmp/dell/" "/tmp/mac/" "/ssh:" "/sudo:" "/scp:")
        recentf-auto-cleanup 600)
  (when (not noninteractive) (recentf-mode 1))

  (defun recentf-save-list ()
    "Save the recent list.
    Load the list from the file specified by `recentf-save-file',
    merge the changes of your current session, and save it back to
    the file."
    (interactive)
    (let ((instance-list (copy-list recentf-list)))
      (recentf-load-list)
      (recentf-merge-with-default-list instance-list)
      (recentf-write-list-to-file)))

  (defun recentf-merge-with-default-list (other-list)
    "Add all items from `other-list' to `recentf-list'."
    (dolist (oitem other-list)
      ;; add-to-list already checks for equal'ity
      (add-to-list 'recentf-list oitem)))

  (defun recentf-write-list-to-file ()
    "Write the recent files list to file.
    Uses `recentf-list' as the list and `recentf-save-file' as the
    file to write to."
    (condition-case error
        (with-temp-buffer
          (erase-buffer)
          (set-buffer-file-coding-system recentf-save-file-coding-system)
          (insert (format recentf-save-file-header (current-time-string)))
          (recentf-dump-variable 'recentf-list recentf-max-saved-items)
          (recentf-dump-variable 'recentf-filter-changer-current)
          (insert "\n \n;;; Local Variables:\n"
    (format ";;; coding: %s\n" recentf-save-file-coding-system)
    ";;; End:\n")
          (write-file (expand-file-name recentf-save-file))
          (when recentf-save-file-modes
            (set-file-modes recentf-save-file recentf-save-file-modes))
          nil)
      (error
       (warn "recentf mode: %s" (error-message-string error)))))
  )

(recentf-mode t)



;; ============== Disable autosave ===============
(setq auto-save-default nil)
(setq make-backup-files t)    ; don't make backup files




(provide 'mydesktop)
