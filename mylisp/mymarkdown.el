;;; mylisp/mymarkdown.el -*- lexical-binding: t; -*-

(use-package markdown-mode
  :mode ("\\.\\(m\\(ark\\)?down\\|md\\)$" . markdown-mode)
  :diminish markdown-mode
  :defer t
  :config)

(provide 'mymarkdown)
