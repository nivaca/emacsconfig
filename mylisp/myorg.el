;;; mylisp/myorg.el -*- lexical-binding: t; -*-

(use-package org
  ;; :defer nil
  :mode ("\\.org\\'" . org-mode)
  :custom
  (org-support-shift-select t)
  (org-ellipsis "⇣")
  (org-src-fontify-natively t)
  (org-src-tab-acts-natively t)
  (org-confirm-babel-evaluate nil)
  (org-export-with-smart-quotes t)
  (org-src-window-setup 'current-window)
  )

(use-package org-bullets
  ;; :defer nil
  :after org
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode))))

(provide 'myorg)
