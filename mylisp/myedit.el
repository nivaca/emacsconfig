;;; mylisp/myedit.el -*- lexical-binding: t; -*-

;; Don't highlight matches with jump-char - it's distracting
(setq jump-char-lazy-highlight-face nil)

;; Easily navigate sillycased words
(global-subword-mode 1)

;; Don't break lines for me, please
(setq-default truncate-lines t)

;; Tabs
(setq tab-width 2)
(setq-default indent-tabs-mode nil)

;; Line spacing
(setq-default line-spacing 3)

;; Standard indent
(setq standard-indent 2)

;; Remove text in active region if inserting text
(delete-selection-mode +1)

;; Never insert tabs
(set-default 'indent-tabs-mode nil)

;; Remove trailing spaces
;; (add-hook 'before-save-hook 'delete-trailing-whitespace)

;; give keyboard focus to help window
(setq-default help-window-select t)

;; winner mode
(use-package winner
  :if (not noninteractive)
  ;;:defer 5
  :bind (("M-n" . winner-redo)
         ("M-p" . winner-undo))
  :custom
  (winner-boring-buffers
   '("*Completions*"
     "*Compile-Log*"
     "*inferior-lisp*"
     "*Fuzzy Completions*"
     "*Apropos*"
     "*Help*"
     "*cvs*"
     "*Buffer List*"
     "*Ibuffer*"
     "*esh command on file*"))
  :config
  (winner-mode 1))



;; ============ aggressive indent ==============
(use-package aggressive-indent
  :ensure
  :init
  (defun
      aggressive-indent-mode-on ()
    (interactive)
    (aggressive-indent-mode 1))
  :hook
  ((prog-mode) . aggressive-indent-mode-on)
  ;; turn it off for python
  ((python-mode) . aggressive-indent-mode)
  )


;; =============== Scrolling ==================
;; Vertical Scroll
(setq scroll-step 1)
(setq scroll-margin 1)
(setq scroll-conservatively 101)
(setq scroll-up-aggressively 0.01)
(setq scroll-down-aggressively 0.01)
(setq auto-window-vscroll nil)
(setq fast-but-imprecise-scrolling nil)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))
(setq mouse-wheel-progressive-speed t)
;; Horizontal Scroll
(setq hscroll-step 1)
(setq hscroll-margin 1)

(when IS-MAC
  ;; sane trackpad/mouse scroll settings
  (setq mac-redisplay-dont-reset-vscroll t
        mac-mouse-wheel-smooth-scroll nil))


;; ============ auto-indent ==============
(use-package auto-indent-mode
  :defer t
  :init
  (setq auto-indent-on-save-file t
        auto-indent-delete-trailing-whitespace-on-save-file t
        auto-indent-untabify-on-save-file t
        auto-indent-indent-style 'aggressive
        auto-indent-key-for-end-of-line-then-newline "<M-return>"
        auto-indent-key-for-end-of-line-insert-char-then-newline "<M-S-return>"
        )
  ;; :config (auto-indent-global-mode)
  :diminish auto-indent-mode
  )

;; ================= Kill Ring ===================
(use-package popup-kill-ring
  :bind ("M-y" . popup-kill-ring))


;; ===============  Yasnippet  ===============
(use-package yasnippet
  :functions yas-global-mode
  :diminish yas-minor-mode
  :defer 3
  :config
  (setq
   yas-verbosity 3
   yas-indent-line nil
   ;; This replaces the default dirs:
   ;; yas-snippet-dirs (list (expand-file-name "snippets" user-emacs-directory))
   ;; This appends:
   yas-snippet-dirs (append yas-snippet-dirs
                            '("~/emacs/snippets"))
   )
  (yas-global-mode t)
  (yas-reload-all)
  )



;; ===================== drag-stuff ====================
(use-package drag-stuff
  :config
  (drag-stuff-global-mode 1)
  (drag-stuff-define-keys)
  ;; this is needed to forcefully diminish the mode
  (define-minor-mode drag-stuff-mode
    "Drag stuff around."
    :init-value nil
    :lighter ""
    :keymap drag-stuff-mode-map)
  )


;; ===================== crux ====================
(use-package crux
  :bind
  (
   ("C-a" . crux-move-beginning-of-line)
   ("C-e" . move-end-of-line)
   ("<home>" . crux-move-beginning-of-line)
   ("<end>" . move-end-of-line)
   ("C-c d" . crux-duplicate-current-line-or-region)
   )
  )

;; =============== expand-region ==============
(use-package expand-region
  :bind ("C-=" . er/expand-region)
  :bind ("C-+" . er/expand-region)
  )



;; only change case when region active
;; ===================================================
(defun ensure-region-active (func &rest args)
  (when (region-active-p)
    (apply func args)))

(advice-add 'upcase-region :around 'ensure-region-active)
(advice-add 'downcase-region :around 'ensure-region-active)



(use-package goto-last-change
  :commands goto-last-change
  ;; S-f1
  )



;; ================= emacs-undo-fu ===================
(use-package undo-fu
  :ensure
  ;; :load-path "~/emacs/otherlisp/emacs-undo-fu"
  :bind
  ("C-z" . undo-fu-only-undo)
  ("C-S-z" . undo-fu-only-redo)
  )

;; ================= avy ========================
(use-package avy
  :defer t
  :ensure
  :bind
  ("M-s" . avy-goto-char)
  ("M-g g" . avy-goto-line)
  )


;; Indicate minibuffer depth
(use-package mb-depth
  :config
  (minibuffer-depth-indicate-mode 1))


(use-package select
  :custom
  (selection-coding-system 'utf-8)
  (select-enable-clipboard t "Use the clipboard"))



;; ============== iedit ==============
(use-package iedit
  :defer t
  )


;; ============== multiple-cursors ==============
(use-package multiple-cursors
  :defer t
  :bind
  (
   ("C-c m t" . mc/mark-all-like-this)
   ("C-c m m" . mc/mark-all-like-this-dwim)
   ("C-c m l" . mc/edit-lines)
   ("C-c m e" . mc/edit-ends-of-lines)
   ("C-c m a" . mc/edit-beginnings-of-lines)
   ("C->" . mc/mark-next-like-this)
   ("C-<" . mc/mark-previous-like-this)
   ("C-c m s" . mc/mark-sgml-tag-pair)
   ("C-c m d" . mc/mark-all-like-this-in-defun)
   ;; ("M-C-<mouse-1>" . mc/add-cursor-on-click)
   )
  )


;; ============== bookmarks ==============
(use-package bookmark+
  :load-path "~/emacs/otherlisp/bookmark+"
  :custom
  (bookmark-version-control t)
  (auto-save-bookmarks t)
  (bookmark-save-flag 1)
  )

;; =========================================
;; Smart delete backward (à la oXygen XML)
(use-package nv-delete-back
  :load-path "~/emacs/otherlisp/nv-delete-back"
  ;;  :bind
  (("C-<backspace>" . nv-delete-back-all)
   ("M-<backspace>" . nv-delete-back))
  )


;; =========================================
;; Smart delete forward (à la oXygen XML)
(use-package nv-delete-forward
  :load-path "~/emacs/otherlisp/nv-delete-forward"
  ;;  :bind
  (("C-<delete>" . nv-delete-forward-all)
   ("M-<delete>" . nv-delete-forward))
  )


;; (use-package hungry-delete
;;;;   :bind
;;   (
;;    ("C-<backspace>" . hungry-delete-backward)
;;    ("C-<delete>" . hungry-delete-forward)
;;    )
;;   )

;; trim extraenus white-space
(use-package ws-butler
  :defer t
  :config
  (add-hook 'emacs-lisp-mode-hook #'ws-butler-mode)
  (add-hook 'latex-mode-hook #'ws-butler-mode)
  )

;; Source: http://www.emacswiki.org/emacs-en/download/misc-cmds.el
(defun revert-buffer-no-confirm ()
  "Revert buffer without confirmation."
  (interactive)
  (revert-buffer t (not (buffer-modified-p)) t)
  )
;; (global-set-key (quote [C-f5]) 'revert-buffer-no-confirm)


(use-package rg
  :defer t
  )


;; ============== good-scroll ==============
;; (use-package good-scroll
;;   :load-path "~/emacs/otherlisp/good-scroll"
;;   :config
;;   (good-scroll-mode 1)
;;   )

;; (use-package buffer-expose
;;;;   ;; :defer t
;;   :config
;;   (buffer-expose-mode 1)
;;   )


(provide 'myedit)
