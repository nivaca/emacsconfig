;; my lsp settings

(use-package lsp-mode
  :ensure t
  :diminish
  :config
  (lsp-register-client
   (make-lsp-client :new-connection (lsp-stdio-connection "digestif")
                    :major-modes '(latex-mode plain-tex-mode)
                    :server-id 'digestif))
  (add-to-list 'lsp-language-id-configuration '(latex-mode . "latex"))
  (setq lsp-prefer-flymake nil)
  :hook (latex-mode . lsp)
  :commands lsp)

(use-package company-lsp
  :diminish
  :config
  (add-to-list 'company-lsp-filter-candidates '(digestif . nil))
  :commands company-lsp)

(provide 'mylsp)
